﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp8
{
    public partial class Form1 : Form
    {
        private List<Rida> LoetudRead = null;
        public Form1()
        {
            InitializeComponent();//CS0116  C# A namespace cannot directly contain members such as fields or methods
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var loetudRead = System.IO.File.ReadAllLines(@"C:\Users\sergo\source\repos\WindowsFormsApp8\WindowsFormsApp8\TextFile1.txt");
            this.LoetudRead = loetudRead.Skip(1).Where(x => x.Trim() != "")
                .Select(x => x.Split(',')
                .Select(y => y.Trim())
                .ToArray())
                .Select(x => new Rida { Nimi = x[0], Distants = x[1] })
                .ToList();
            this.dataGridView1.DataSource = LoetudRead;
            this.button2.Visible = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(LoetudRead != null)
            {
                LoetudRead = LoetudRead.OrderBy(x => x.Nimi).ToList();
                this.dataGridView1.DataSource = LoetudRead;
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
